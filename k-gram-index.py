from os import listdir
from os.path import isfile, join
from collections import defaultdict

mypath = ''

files = [f for f in listdir(mypath) if isfile(join(mypath, f))]
files.sort()

docs = list()


def findEditDistance(s1, s2):
    if len(s1) > len(s2):
        s1, s2 = s2, s1

    distances = range(len(s1) + 1)
    for i2, c2 in enumerate(s2):
        distances_ = [i2 + 1]
        for i1, c1 in enumerate(s1):
            if c1 == c2:
                distances_.append(distances[i1])
            else:
                distances_.append(
                    1 + min((distances[i1], distances[i1 + 1], distances_[-1])))
        distances = distances_
    return distances[-1]


# reading all words
for tempFile in files:
    with open(mypath + '/' + tempFile) as f:
        # words = [word[0] + " " + word[1]
                 # for line in f for word in zip(line.split(), line.split()[1:])]
        words = [word for line in f for word in line.split()]
        # print(words)
        docs.append(words)
        # print(docs)
        # docs.append([w: for w in words])

# building inverted index
# print(docs)
invertedIndex = defaultdict(list)
for index, text in enumerate(docs):
    for word in text:
        invertedIndex[word].append(index)

# print(invertedIndex.items())

# kGrams is a set of k-grams in all the words in the inverted index
# KGramIndex contains the list of all the words for a given k-gram
gram = 3
kGrams = set()
kGramIndex = defaultdict(set)
kGramIndex['randomInitialKey'] = 'randomInitialKey'
# kGrams = list()

# docs = [[words in doc1], [words in doc2], ...]
# words = [words in the lastdoc]
# trying to set words = list of words in docs breaks kGrams.add(word[i:i + gram])
# newList is a hack whose sole purpose is to facilitate k-gram index builds 
newList = list()
for lists in docs:
    for t in lists:
        newList.append(t)
# print(newList)

# should probably write a k-gramify function considering the query also needs one
# for doc in docs:
for word in newList:
    i = 0
    while True:
        if word[i:i + gram] == '':
            break

        # use the append along with kGrams = list()
        # to test for proper initialisation of k-grams
        # kGrams.append(word[i:i+gram])
        # print(word[i:i + gram])
        # print('adding ' + word[i:i + gram] + ' to the list of k-grams')
        kGrams.add(word[i:i + gram])
        kGramIndex[word[i:i + gram]].add(word)
        i = i + 1

# print(kGrams)
# print(kGramIndex)

# query = 'kusanagi in a boat'
query = 'kusnagi in a bot' # wanted 'kusanagi in a boat'
# query3 = 'is dolores in westwold'
# query4 = 'single point of failure'

keywords = [term for term in query.split()]
qKGrams = list()
minDistance = 200  # just som random number to start comparing with
suggestedWord = ''
suggestions = dict()  # enteredKeyWord: suggestedKeyWord

for keyword in keywords:
    # print(keyword)
    i = 0
    while True:
        if keyword[i:i + gram] == '':
            break
        qKGrams.append(keyword[i:i + gram])

        # print(qKGrams)
        for partOfWord in qKGrams:
            if keyword not in kGramIndex[partOfWord]:
                # print('cant find word in index')
                for w in qKGrams:
                    for k in kGramIndex.keys():
                        if w == k:
                            # it is necessary to compare the query k-gram and the indexed k-gram
                            # because they might not always be the same. some random word that
                            # might not exist in the document
                            for tempWord in kGramIndex[k]:
                                editDistance = findEditDistance(
                                    keyword, tempWord)
                                if editDistance < minDistance:
                                    minDistance = editDistance
                                    suggestedWord = tempWord
                                    suggestions[keyword] = suggestedWord
                # print('did you mean ' + suggestedWord + ' instead of ' + keyword)

        i = i + 1
    qKGrams.clear()
    # print(qKGrams)

# lol that had 6 nested loops. so ded
# print(qKGrams)
print(suggestions)

finalQuery = list()
for keyword in keywords:
    if keyword in suggestions.keys():
        keyword = suggestions[keyword]
    finalQuery.append(keyword)
    finalQuery.append(' ')

finalQueryString = ''.join(finalQuery)
finalQueryString = finalQueryString[:-1]
# print(finalQueryString)

if finalQueryString != query:
    print('did you mean \'' + finalQueryString +
          '\' instead of \'' + query + '\'')
else:
    print('no spelling mistakes detected')
