from os import listdir
from os.path import isfile, join
from collections import defaultdict
from math import log10
from math import sqrt

mypath = ''

files = [f for f in listdir(mypath) if isfile(join(mypath, f))]
files.sort()

docs = list()
documentLength = dict()
noOfDocs = 0
documents = list()
fileNames = list()

# reading all words
for index, tempFile in enumerate(files):
    with open(mypath + '/' + tempFile) as f:
        fileNames.append(tempFile)
        # words = [word[0] + " " + word[1]
        # for line in f for word in zip(line.split(), line.split()[1:])]
        words = [word for line in f for word in line.split()]

        # to make [{word1:{'tfRaw':0,'idf':0,} in doc1}, {words in doc2}, ...]
        document = {word: {'tfRaw': 0, 'idf': 0,
                           'tfFinalWeight': 0, 'normalised': 0} for word in words}
        # print(document)
        documents.append(document)

        documentLength[index] = len(words)
        docs.append(words)
        noOfDocs = index
noOfDocs = noOfDocs + 1

# building inverted index
# print(docs)
invertedIndex = defaultdict(list)
for index, text in enumerate(docs):
    for word in text:
        invertedIndex[word].append(index)

# print(invertedIndex)
# print(documentLength)

allDocs = set()
for a in invertedIndex.values():
    for b in a:
        allDocs.add(b)

invertedIndex2 = defaultdict(list)
documentCount = defaultdict(int)
wordCount = defaultdict(int)  # word: total frequency in all docs

for k in invertedIndex.keys():
    for v in allDocs:
        #         # print(k + ', ' + str(v) + ': ' + str(invertedIndex[k].count(v)))
        wordCount[k] = wordCount[k] + invertedIndex[k].count(v)
        if invertedIndex[k].count(v) > 0:
            documentCount[k] = documentCount[k] + 1
        invertedIndex2[k].append((v, invertedIndex[k].count(v)))

# print(invertedIndex2)
# print('\n')
# print(documentCount)

query = input('Search => ')
# query = 'is dolores in westworld in'
query = query.split()
queryWordCount = dict()
queryWordWeight = dict()
qidf = dict()
queryFinalWeight = dict()
queryNormalised = dict()

# query processing
for t in query:
    queryWordCount[t] = query.count(t)

    if queryWordCount[t] > 0:
        queryWordWeight[t] = 1 + log10(queryWordCount[t])
    else:
        queryWordWeight[t] = 0

    if documentCount[t] == 0:
        qidf[t] = 0
    else:
        qidf[t] = log10(noOfDocs / documentCount[t])
    queryFinalWeight[t] = queryWordWeight[t] * qidf[t]

# document weight matrix
sum = 0
for t in query:
    for t2 in queryFinalWeight.values():
        sum = sum + float(t2)**2
    queryNormalised[t] = queryFinalWeight[t] / (sqrt(sum))

for index, document in enumerate(documents):
    for k in document.keys():
        document[k]['tfRaw'] = invertedIndex2[k][index][1]
        document[k]['idf'] = 1
        if invertedIndex2[k][index][1] > 0:
            document[k]['tfFinalWeight'] = 1 + \
                log10(invertedIndex2[k][index][1])
        else:
            document[k]['tfFinalWeight'] = 0

# document normalisation
sum = 0
for index, document in enumerate(documents):
    for k, v in document.items():
        for k2, v2 in document.items():
            sum = sum + float(v2['tfFinalWeight'])**2
        v['normalised'] = v['tfFinalWeight'] / sqrt(sum)

cosProduct = list()
cosProductDocument = 0

# matching query against documents
for document in documents:
    # print(document)
    for t in query:
        # print(document) returns {words:{normalised:value}}
        try:
            # print(queryNormalised[t])
            # print(document[t]['normalised'])
            cosProductDocument = cosProductDocument + \
                queryNormalised[t] * document[t]['normalised']
            # print(cosProductDocument)
        except KeyError:
            # print(0)
            continue
    cosProduct.append(cosProductDocument)
    cosProductDocument = 0

print(query)
print(queryNormalised)
results = [(index, value) for index, value in enumerate(cosProduct)]
result = list()
# replace index with filenames
for index, value in results:
    # print(index,value)
    temp = fileNames[index]
    index = temp
    result.append((index, value))
result.sort(key=lambda x: x[1], reverse=True)
print(result)
